from django.urls import path
from accounts.views import login_user, user_logout, sign_up_user


urlpatterns = [
    path("login/", login_user, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", sign_up_user, name="signup"),
]
